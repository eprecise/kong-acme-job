package eprecise.tools.jobs.kcj

import java.nio.file.Files
import java.nio.file.Paths
import java.util.concurrent.Executors
import java.util.logging.Logger

class Certbot(private val email: String) {
	private val certfileName = "kong-acme-job-certs"
	private val certsPath = Paths.get("/etc/letsencrypt/live/${certfileName}");

	private val log = Logger.getGlobal()
	private val executor = Executors.newFixedThreadPool(2)
	private val runtime = Runtime.getRuntime()
	private val commandTemplate = "certbot certonly --agree-tos --standalone --preferred-challenges http -n -m ${email} --cert-name ${certfileName} %s"

	private fun commandFor(domains: List<String>) = commandTemplate.format(domains.map { "-d ${it}" }.joinToString(" "))

	fun exec(domains: List<String>): Boolean {
		val process = runtime.exec(commandFor(domains).also { println(it) })
		executor.run { process.inputStream.bufferedReader().use { it.lines().forEach { log.info(it) } } }
		executor.run { process.errorStream.bufferedReader().use { it.lines().forEach { log.severe(it) } } }
		return process.waitFor().also { log.info({ "Retorno de Execução: ${it}" }) } == 0
	}

	fun certificateOf(domain: String): Certificate {
		return Certificate(
				domain = domain,
				key = Files.readAllLines(this.certsPath.resolve("privkey.pem")).joinToString("\n"),
				cert = Files.readAllLines(this.certsPath.resolve("fullchain.pem")).joinToString("\n")
		)
	}
}
package eprecise.tools.jobs.kcj

data class Certificate(
		val domain: String,
		val key: String,
		val cert: String
)
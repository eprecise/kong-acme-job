package eprecise.tools.jobs.kcj.kong

import java.lang.AutoCloseable
import javax.ws.rs.client.WebTarget
import javax.ws.rs.core.Form
import javax.ws.rs.client.Entity.form
import java.util.logging.Logger
import javax.ws.rs.core.Response.Status
import java.net.URI

class CheckEndpoint(private val name: String = "kong-acme-job-endpoint",
					private val currentHost: URI,
					private val domains: List<String>,
					private val target: WebTarget) : AutoCloseable {
	private val log = Logger.getGlobal()
	private val checkPath = "/.well-known/acme-challenge"

	var open = false
		private set

	init {
		log.fine({ "(CheckEndpoint) HTTP POST /apis with ${name} for ${domains}" })
		val form = form(Form()
				.param("name", name)
				.param("uris", checkPath)
				.param("upstream_url", currentHost.resolve(checkPath).toString())
				.param("hosts", domains.joinToString(","))
				.param("methods", "GET,OPTIONS"))
		val response = this.target.request().post(form)
		try {
			if (response.statusInfo.family == Status.Family.SUCCESSFUL) {
				log.fine("(CheckEndpoint) Criado com sucesso")
				this.open = true
			} else {
				log.severe("Falha ao criar CheckEndpoint [${response.status}]")
			}
		} finally {
			response.close()
		}
	}

	override fun close() {
		log.fine({ "(CheckEndpoint) HTTP DELETE /apis/${name} " })
		val response = this.target.path(name).request().delete()
		this.open = false
		try {
			if (response.statusInfo.family == Status.Family.SUCCESSFUL) {
				log.fine("(CheckEndpoint) Removido com sucesso")
			} else {
				log.severe("Falha ao remover CheckEndpoint [${response.status}]")
			}
		} finally {
			response.close()
		}
	}
}
package eprecise.tools.jobs.kcj.kong

data class Api(
		val name: String = "",
		val hosts: List<String> = listOf<String>()
)
package eprecise.tools.jobs.kcj

import eprecise.tools.jobs.kcj.kong.Api
import java.net.URI
import java.util.logging.Logger
import javax.json.JsonObject
import javax.json.JsonString
import javax.ws.rs.client.Client
import javax.ws.rs.client.ClientBuilder
import javax.ws.rs.client.Entity.form
import javax.ws.rs.core.Form
import javax.ws.rs.core.MediaType.APPLICATION_JSON
import javax.ws.rs.core.Response.Status
import javax.ws.rs.core.Response.Status.CONFLICT
import javax.ws.rs.core.Response.Status.OK
import org.glassfish.json.jaxrs.JsonValueBodyReader
import org.glassfish.json.jaxrs.JsonValueBodyWriter
import eprecise.tools.jobs.kcj.kong.CheckEndpoint

class KongAdmin(private val host: URI,
				private val client: Client = ClientBuilder.newBuilder()
						.register(JsonValueBodyReader::class.java)
						.register(JsonValueBodyWriter::class.java)
						.build()
) {
	private val log = Logger.getGlobal()

	private val target = client.target(host)

	private val certificates get() = target.path("certificates")

	private val apis get() = target.path("apis")

	fun getCerts(): List<Certificate> = this.certificates.request().accept(APPLICATION_JSON).get(JsonObject::class.java)
			.let { it.getJsonArray("data") }
			.map { it.asJsonObject() }
			.flatMap {
				val r = it;
				it.getJsonArray("snis")
						?.getValuesAs(JsonString::class.java)
						?.map { it.getString() }
						?.map { Certificate(domain = it, key = r.getString("key"), cert = r.getString("cert")) }
						?: listOf()
			}

	fun saveCert(cert: Certificate) =
			when (this.insertCert(cert)) {
				CONFLICT -> when (this.updateCert(cert)) {
					OK -> true
					else -> false
				}
				OK -> true
				else -> false
			}

	private fun insertCert(cert: Certificate): Status {
		log.fine({ "HTTP POST ${certificates} for ${cert.domain}" })
		val form = form(Form()
				.param("snis", cert.domain)
				.param("key", cert.key)
				.param("cert", cert.cert))
		val response = certificates.request().accept(APPLICATION_JSON).post(form);
		val status = response.status
		response.close()
		log.fine({ "Response: ${status}" })
		return Status.fromStatusCode(status)
	}

	private fun updateCert(cert: Certificate): Status {
		log.fine({ "HTTP PATCH ${certificates} for ${cert.domain}" })
		val form = form(Form()
				.param("key", cert.key)
				.param("cert", cert.cert))
		val response = certificates.path(cert.domain).request().accept(APPLICATION_JSON).method("PATCH", form)
		val status = response.status
		response.close()
		log.fine({ "Response: ${status}" })
		return Status.fromStatusCode(status)
	}

	fun getApis() = this.apis.request().accept(APPLICATION_JSON).get(JsonObject::class.java)
			.let { it.getJsonArray("data") }
			.map { it.asJsonObject() }
			.map {
				Api(name = it.getString("name", ""),
						hosts = it.getJsonArray("hosts")
								?.getValuesAs(JsonString::class.java)
								?.map { it.getString() }
								?.toList()
								?: listOf())
			}

	fun startCheckEndpoint(domains: List<String>, currentHost: URI): CheckEndpoint {
		return CheckEndpoint(currentHost = currentHost, domains = domains, target = this.apis);
	}
}



package eprecise.tools.jobs.kcj

import org.apache.commons.cli.DefaultParser
import org.apache.commons.cli.Options
import org.apache.commons.cli.HelpFormatter
import org.apache.commons.cli.OptionGroup
import java.net.URI

class Args(val args: Array<String>) {

	val options = Options()
			.addOption(OPT_HELP, "help", false, "Show this help message")
			.addOption(OPT_KONG_ADMIN_HOST, "kong", true, "Kong Admin API Host")
			.addOption(OPT_CURRENT_HOST, "current", true, "This host for HTTP(S) access")
			.addOption(OPT_EMAIL, "email", true, "Email associated with Let's Encrypt ")
			.addOption(OPT_MODE, "mode", true, """Modo de operação:
			                                          hosts  - Cria ou atualiza certificado para todos os domínios do Kong mais os parametrizados por -${OPT_DOMAINS}
			                                          certificates (padrão) - Atualiza todos os certificados do Let's Encrypt no Kong mais os parametrizados por -${OPT_DOMAINS}
			                                          params - Ignora domínio do Kong e trabalha somente pelos parametrizados por -${OPT_DOMAINS}
			                                   """)
			.addOption(OPT_DOMAINS, "domains", true, "Relação de domínios a renovar")
			.addOption(OPT_DOMAINS_TO_IGNORE, "ignore", true, "Relação de domínios a ignorar")
			.addOption(OPT_DRY_RUN, "dry-run", false, "Apenas testa a execução do comando")

	val parsed = DefaultParser().parse(options, args)

	val help get() = this.parsed.hasOption(OPT_HELP)
	val kongHost: URI? get() = this.parsed.getOptionValue(OPT_KONG_ADMIN_HOST).let { URI(it) }
	val currentHost: URI? get() = this.parsed.getOptionValue(OPT_CURRENT_HOST).let { URI(it) }
	val email: String? get() = this.parsed.getOptionValue(OPT_EMAIL)
	val mode: DomainSource get() = this.parsed.getOptionValue(OPT_MODE)?.let { DomainSource.fromArg(it) } ?: DomainSource.CERTIFICATES
	val domains get() = this.parsed.getOptionValue(OPT_DOMAINS)?.split(',')?.toList() ?: listOf()
	val ignoreDomains get() = this.parsed.getOptionValue(OPT_DOMAINS_TO_IGNORE)?.split(',')?.toList() ?: listOf()

	fun printHelpMenu() {
		HelpFormatter().printHelp("kong-certbot-job", "Kong Certbot JOB", options, "e-Precise Soluções")
	}

	companion object {
		const val OPT_HELP = "h"
		const val OPT_KONG_ADMIN_HOST = "k"
		const val OPT_CURRENT_HOST = "c"
		const val OPT_EMAIL = "e"
		const val OPT_MODE = "m"
		const val OPT_DOMAINS = "d"
		const val OPT_DOMAINS_TO_IGNORE = "i"
		const val OPT_DRY_RUN = "dr"
	}
}


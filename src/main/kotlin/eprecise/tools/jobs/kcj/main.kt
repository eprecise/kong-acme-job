@file:JvmName("Main")

package eprecise.tools.jobs.kcj

import org.apache.commons.cli.ParseException


fun main(args: Array<String>) {

	try {
		val params = Args(args);
		if (params.help) {
			params.printHelpMenu();
		} else {
			Job(params).run()
		}
	} catch(e: ParseException) {
		e.printStackTrace()
	}
}
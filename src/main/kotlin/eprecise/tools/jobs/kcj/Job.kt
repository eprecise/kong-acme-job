package eprecise.tools.jobs.kcj

import eprecise.tools.jobs.kcj.DomainSource.*
import java.util.logging.Logger

class Job(val params: Args) {
	val kadm = KongAdmin(params.kongHost!!)
	val log = Logger.getGlobal();

	fun run() {
		log.info({ "Iniciando JOB" });
		log.info({ "Kong Admin Host (k): ${params.kongHost}" });
		log.info({ "Current Host (c): ${params.currentHost}" });
		log.info({ "Let's Encrypt email (e): ${params.email}" });
		log.info({ "Modo (m): ${params.mode}" });
		log.info({ "Domínios para renovar (d): ${params.domains}" });
		log.info({ "Domínios para ignorar (i): ${params.ignoreDomains}" });

		when (params.mode) {
			HOSTS -> fromHostsMode()
			CERTIFICATES -> fromCertificatesMode()
			PARAMS -> execOnlyByParams()
		}
	}

	fun fromHostsMode() {
		log.info("Buscando domínios em ${params.kongHost}/apis para renovação de certificado")
		val domains = kadm.getApis().flatMap { it.hosts }
		log.info("Encontrados ${domains.size} domínios: ${domains}")
		this.renewDomains(domains)
	}

	fun fromCertificatesMode() {
		log.info("Buscando certificados em ${params.kongHost}/certificates para renovação")
		val certs = kadm.getCerts().map { it.domain }
		log.info("Encontrados ${certs.size} certificados para os domínios ${certs}")
		this.renewDomains(certs)
	}

	fun execOnlyByParams() {
		renewDomains(parseDomains(listOf()))
	}

	private fun parseDomains(domains: List<String>) = domains.union(params.domains).minus(params.ignoreDomains).toList()

	private fun renewDomains(domains: List<String>) {
		log.info({ "Renovando ${domains.size} domínio(s): ${domains}" })
		kadm.startCheckEndpoint(domains, this.params.currentHost!!).use {
			if (it.open) {
				val acmeClient = Certbot(params.email!!)

				val result = acmeClient.exec(domains)
				if (result) {
					log.info({ "ACME Client executado com sucesso, atualizando no kong" })
					domains.map(acmeClient::certificateOf).onEach { log.info({ "Atualizando ${it.domain}" }) }.onEach { kadm.saveCert(it) }
				} else {
					log.severe({ "Falha ao executar o cliente ACME" })
				}
			} else {
				log.severe({ "Falha ao criar o CheckEndpoint" })
			}
		}
	}
}
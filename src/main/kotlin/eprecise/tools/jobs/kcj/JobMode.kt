package eprecise.tools.jobs.kcj

enum class DomainSource {
	HOSTS,
	CERTIFICATES,
	PARAMS;

	companion object {
		fun fromArg(arg: String?) = if (arg == null) null else DomainSource.valueOf(arg.toUpperCase())
	}
}
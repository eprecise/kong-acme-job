FROM openjdk:8-jre-alpine

MAINTAINER Clécius J. Martinkoski <clecius@e-precise.com.br>

ARG JAR_FILE

EXPOSE 80 443

VOLUME /etc/letsencrypt /var/lib/letsencrypt

RUN apk add --no-cache certbot

COPY ./dockerRun.sh /opt/entrypoint.sh

RUN echo ${JAR_FILE}

COPY ${JAR_FILE} /opt/job.jar

LABEL version=${VERSION}

ENTRYPOINT ["/opt/entrypoint.sh"]
